package com.nueda.shortenurl.service;

import com.nueda.shortenurl.CommonUtil;
import com.nueda.shortenurl.entity.Url;
import com.nueda.shortenurl.repository.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.UnsupportedEncodingException;
import java.util.Optional;

@Service
public class UrlService {

    @Autowired
    private UrlRepository repository;

    /**
     * method to check if the alias exists in the db
     * @param alias
     * @return
     */
    public boolean doesAliasExist(final String alias){
        return repository.existsById(alias);
    }

    /**
     * method to persist the Url object to the db via the repository
     * @param url
     * @return
     */
    public Url createShortenUrl(Url url){
        return repository.save(url);
    }

    /**
     * method to check if we have an alias - shorten url stored and if it is not expired so us to perform the necessary redirect
     * @param alias
     * @return
     */
    public String findByAliasAndIsNotExpired(final String alias) {
        Optional<Url> url =  repository.findByAliasAndIsNotExpired(alias);
        if(url.isPresent()){
            try {
                return CommonUtil.decodeUrl(url.get().getOriginalUrl());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
