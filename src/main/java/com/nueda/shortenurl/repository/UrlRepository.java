package com.nueda.shortenurl.repository;

import com.nueda.shortenurl.entity.Url;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

public interface UrlRepository extends JpaRepository<Url,String> {

    @Query(value = "SELECT * FROM nueda_shortenurl_db.url WHERE alias = ?1 and current_timestamp <= expiration_date", nativeQuery = true)
    Optional<Url> findByAliasAndIsNotExpired(final String alias);

}
