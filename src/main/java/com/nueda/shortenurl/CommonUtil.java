package com.nueda.shortenurl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

/**
 * Helper Class that provide basic functionality methods
 */
public class CommonUtil {
    public static final long EXPIRATION_DAYS_DURATION = TimeUnit.DAYS.toMillis(100);//this variable will be added when the url row will be created in the database and will give an expiration date creationDate+100 days
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * method to forge an expirationDate to be persisted
     * @return
     */
    public static java.sql.Timestamp getExpirationDate(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis() + CommonUtil.EXPIRATION_DAYS_DURATION);
        return Timestamp.valueOf(sdf.format(timestamp));
    }

    /**
     * method to get current Date (used for testing)
     * @return
     */
    public static java.sql.Timestamp getCurrentDate(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return Timestamp.valueOf(sdf.format(timestamp));
    }

    /**
     * Method used to generate the unique hash based on the original url and an incremental id number(based on the sequence)
     * @param encodedOriginalUrl
     * @return
     */
    public static String hashOriginalUrl(final String encodedOriginalUrl,final int dice){
        //we will perform a unique SHA-256 hash
        String sha256hash = sha256(encodedOriginalUrl);
        //and then we will apply a base62 encoding in order to shrink the visual fragment of the shortenedurl
        String base62Str = base62(sha256hash, dice);
        return base62Str;
    }

    /**
     * Helper class to perform SHA-256 hashing
     * @param base
     * @return
     */
    public static String sha256(final String base) {
        try{
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Helper class to perform base62 encoding----> 62^7 combinations (given the fact that we would like a 7 character hashed shorten url fragment)
     * @param base
     * @param deci a random number we will use in order to help us calculate the base62 hash
     * @return
     */
    public static String base62(final String base,int deci)  {
        StringBuilder hash_str = new StringBuilder("");

        while(deci>0){
            hash_str.insert(0,base.charAt(deci % 62));
            deci = deci / 62;
        }

        return hash_str.toString();
    }

    /**
     * Method used to encode the original url(even if it is already encoded)
     * @param originalUrl
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String encodeOriginalUrl(final String originalUrl) throws UnsupportedEncodingException {
        //We decode and re-encode in order to ensure that the user will not paste an already encoded url
        String decodedUrl = URLDecoder.decode(originalUrl, String.valueOf(StandardCharsets.UTF_8));
        return URLEncoder.encode(decodedUrl, String.valueOf(StandardCharsets.UTF_8));
    }

    /**
     * Method to decode an encoded Url
     * @param encodedUrl
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String decodeUrl(final String encodedUrl) throws UnsupportedEncodingException {
        return URLDecoder.decode(encodedUrl, String.valueOf(StandardCharsets.UTF_8));
    }


}
