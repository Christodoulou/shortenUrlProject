package com.nueda.shortenurl.controller;

import com.nueda.shortenurl.CommonUtil;
import com.nueda.shortenurl.entity.Url;
import com.nueda.shortenurl.exception.BadRequestException;
import com.nueda.shortenurl.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ThreadLocalRandom;

@RestController
public class UrlController {

    @Autowired
    private UrlService service;

    @PostMapping("/createShortenedUrl")
    public ResponseEntity<?> createShortenURL(@RequestBody Url url){
        String originalUrl = url.getOriginalUrl();

        try {
            //encode the original Url
            String encodedUrl = CommonUtil.encodeOriginalUrl(originalUrl);
            url.setOriginalUrl(encodedUrl);
            //we check if the user has sent a custom alias we will be using it for the shortened part of the url (hash)
            if(url.getAlias()==null){
                //hash the encoded url
                //here if we pass the same number for deci(e.g 999( we will prevent other users create shortened urls for the same url (so i put a random range here and in case we have a conflict this will be handled)
                //as mentioned in the Notes.txt depending on how we want to software to behave if we want all users to be able to create shorten urls for the same url we could add an incremental number before we hash the url(room for discussion depending on how we want our app to behave)
                String hashedUrl = CommonUtil.hashOriginalUrl(encodedUrl, ThreadLocalRandom.current().nextInt(500,1999));
                url.setAlias(hashedUrl);
            }
            //check if the alias already exists(either when manually entered or when is automatically calculated)
            if(service.doesAliasExist(url.getAlias())){
                throw new BadRequestException("Alias already exists");
            }

            //we add the expiration date based on the server time + duration
            url.setExpirationDate(CommonUtil.getExpirationDate());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok(service.createShortenUrl(url));
    }

    @GetMapping("/{alias}")
    public ResponseEntity<?> performRedirect(@PathVariable String alias) throws URISyntaxException {
        //check if the shortened url exists and if it is still valid - not expired
        String redirectUrl = service.findByAliasAndIsNotExpired(alias);

        //if the shorten url does not exist or is expired
        if(redirectUrl==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(new URI(redirectUrl));
        httpHeaders.setCacheControl("no-cache");
        return new ResponseEntity<>(httpHeaders, HttpStatus.MOVED_PERMANENTLY);
    }

}
