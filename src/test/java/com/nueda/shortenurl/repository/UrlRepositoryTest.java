package com.nueda.shortenurl.repository;

import com.nueda.shortenurl.CommonUtil;
import com.nueda.shortenurl.entity.Url;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UrlRepositoryTest {

    @Autowired
    private UrlRepository urlRepositoryUnderTest;

    @Test
    void itShouldCheckIfAliasExistsAndIsNotExpired() {
        //given
        Url tempUrl = new Url();
        final String CUSTOM_ALIAS = "myCustomAlias";
        tempUrl.setAlias(CUSTOM_ALIAS);
        tempUrl.setOriginalUrl("www.myActualUrl.com");
        tempUrl.setCreationDate(CommonUtil.getCurrentDate());
        tempUrl.setExpirationDate(CommonUtil.getExpirationDate());
        tempUrl.setIpAddress("192.168.1.1");
        urlRepositoryUnderTest.save(tempUrl);
        //when
        Optional<Url> repUrl = urlRepositoryUnderTest.findByAliasAndIsNotExpired(CUSTOM_ALIAS);
        //then
        assertEquals(CUSTOM_ALIAS,repUrl.get().getAlias());

        //clean up test record from db
        urlRepositoryUnderTest.delete(tempUrl);
    }
}