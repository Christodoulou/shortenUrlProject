package com.nueda.shortenurl.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.nueda.shortenurl.CommonUtil;
import com.nueda.shortenurl.entity.Url;
import com.nueda.shortenurl.model.UrlRequest;
import com.nueda.shortenurl.service.UrlService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import java.nio.charset.Charset;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UrlController.class)
class UrlControllerTest {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UrlService service;

    @Test
    void createShortenURL() throws Exception {
        UrlRequest objReq = new UrlRequest();
        objReq.setAlias("testAlias");
        objReq.setOriginalUrl("www.youtube.com");
        objReq.setIpAddress("156.76.28.1");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(objReq );
        mvc.perform(post("/createShortenedUrl").contentType(APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    void createShortenURLWithoutAlias() throws Exception {
        UrlRequest objReq = new UrlRequest();
        objReq.setOriginalUrl("www.youtube.com");
        objReq.setIpAddress("156.76.28.1");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(objReq );
        mvc.perform(post("/createShortenedUrl").contentType(APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    void createShortenURLCheckWithAliasExist() throws Exception {
        Mockito.when(service.doesAliasExist("testAlias")).thenReturn(true);
        UrlRequest objReq = new UrlRequest();
        objReq.setAlias("testAlias");
        objReq.setOriginalUrl("www.youtube.com");
        objReq.setIpAddress("156.76.28.1");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(objReq );
        mvc.perform(post("/createShortenedUrl").contentType(APPLICATION_JSON_UTF8)
                .content(requestJson));
    }

    @Test
    void successfulRedirect() throws Exception {
        Mockito.when(service.findByAliasAndIsNotExpired("myCustomAlias")).thenReturn("www.myActualUrl.com");
        Url tempUrl = new Url();
        final String CUSTOM_ALIAS = "myCustomAlias";
        tempUrl.setAlias(CUSTOM_ALIAS);
        tempUrl.setOriginalUrl("www.myActualUrl.com");
        tempUrl.setCreationDate(CommonUtil.getCurrentDate());
        tempUrl.setExpirationDate(CommonUtil.getExpirationDate());
        tempUrl.setIpAddress("192.168.1.1");

        RequestBuilder request = get("/"+CUSTOM_ALIAS);
        MvcResult result = mvc.perform(request).andReturn();
        assertEquals(HttpStatus.MOVED_PERMANENTLY.value(),result.getResponse().getStatus());
    }

    @Test
    void nonSuccessfulRedirect() throws Exception {
        Mockito.when(service.findByAliasAndIsNotExpired("aNonExistingAlias")).thenReturn(null);
        Url tempUrl = new Url();
        final String CUSTOM_ALIAS = "aNonExistingAlias";
        tempUrl.setAlias(CUSTOM_ALIAS);
        tempUrl.setOriginalUrl("www.myActualUrl.com");
        tempUrl.setCreationDate(CommonUtil.getCurrentDate());
        tempUrl.setExpirationDate(CommonUtil.getExpirationDate());
        tempUrl.setIpAddress("192.168.1.1");

        RequestBuilder request = get("/"+CUSTOM_ALIAS);
        MvcResult result = mvc.perform(request).andReturn();
        assertEquals(HttpStatus.NOT_FOUND.value(),result.getResponse().getStatus());
    }

}