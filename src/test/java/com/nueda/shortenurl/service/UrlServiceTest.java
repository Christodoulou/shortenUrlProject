package com.nueda.shortenurl.service;

import com.nueda.shortenurl.entity.Url;
import com.nueda.shortenurl.repository.UrlRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import java.sql.Timestamp;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;


@RunWith(SpringRunner.class)
class UrlServiceTest {

    @Mock
    private UrlRepository urlRepository;
    private AutoCloseable autoCloseable;
    @InjectMocks
    private UrlService urlServiceUnderTest;
    @Mock
    private Url tempUrl;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        Mockito.when(tempUrl.getAlias()).thenReturn("myCustomAlias");
        Mockito.when(tempUrl.getOriginalUrl()).thenReturn("www.myActualUrl.com");
        Mockito.when(tempUrl.getIpAddress()).thenReturn("192.168.1.1");
        Mockito.when(tempUrl.getCreationDate()).thenReturn(new Timestamp(1L));
        Mockito.when(tempUrl.getExpirationDate()).thenReturn(new Timestamp(2L));

    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void doesAliasExist() {
        final String CUSTOM_ALIAS = "myCustomAlias";
        Mockito.when(urlRepository.existsById(CUSTOM_ALIAS)).thenReturn(true);
        Boolean exists = urlServiceUnderTest.doesAliasExist(CUSTOM_ALIAS);
        assertEquals(exists,true);
    }

    @Test
    void doesAliasNotExist() {
        final String CUSTOM_ALIAS = "myCustomAlias";
        Mockito.when(urlRepository.existsById(CUSTOM_ALIAS+"1")).thenReturn(true);
        Boolean exists = urlServiceUnderTest.doesAliasExist(CUSTOM_ALIAS);
        assertEquals(exists,false);
    }

    @Test
    void createShortenUrl() {
        Mockito.when(urlRepository.save(tempUrl)).thenReturn(tempUrl);
        Url pUrl =urlServiceUnderTest.createShortenUrl(tempUrl);
        assertEquals(tempUrl.getAlias(),pUrl.getAlias());
    }

    @Test
    void findByAliasAndIsNotExpired() {
        Mockito.when(urlRepository.findByAliasAndIsNotExpired(tempUrl.getAlias())).thenReturn(Optional.ofNullable(tempUrl));
        String redirectedUrl = urlServiceUnderTest.findByAliasAndIsNotExpired(tempUrl.getAlias());
        assertEquals(tempUrl.getOriginalUrl(),redirectedUrl);
    }

    @Test
    void findByAliasAndIsNotExpiredIfUrlNotPresent() {
        Mockito.when(urlRepository.findByAliasAndIsNotExpired(tempUrl.getAlias())).thenReturn(Optional.ofNullable(null));
        String redirectedUrl = urlServiceUnderTest.findByAliasAndIsNotExpired(tempUrl.getAlias());
        assertEquals(null,redirectedUrl);
    }
}