package com.nueda.shortenurl;

import org.junit.jupiter.api.Test;
import java.io.UnsupportedEncodingException;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test Class for CommonUtil Class
 */
class CommonUtilTest {

    @Test
    void hashOriginalUrl() throws UnsupportedEncodingException {
        final String originalUrl="http://www.google.com/dsadsadashkjasdhjasdhsdajklhsdlahasdlkhjdskalsdlakhjasdlhdslkhdslakdhsal";
        final int dice = 999888888;
        final String encodedOriginalUrl = CommonUtil.encodeOriginalUrl(originalUrl);
        assertEquals("d34c4c",CommonUtil.hashOriginalUrl(encodedOriginalUrl,dice));
    }

    @Test
    void sha256() {
        final String encodedUrl="http%3A%2F%2Fwww.nueda.com";
        assertEquals("adef5225c8f9095673eb23fb2216cb06f6c61128f9e3850fee335ba2fc41c3b1",CommonUtil.sha256(encodedUrl));
    }

    @Test
    void base62() {
        final String sha256 = "253d142703041dd25197550a0fc11d6ac03befc1e64a1320009f1edf400c39ad";
        assertEquals("57",CommonUtil.base62(sha256,999));
    }

    @Test
    void encodeOriginalUrl() throws UnsupportedEncodingException {
        final String originalUrl = "http://www.nueda.com";
        assertEquals("http%3A%2F%2Fwww.nueda.com",CommonUtil.encodeOriginalUrl(originalUrl));
    }

    @Test
    void decodeUrl() throws UnsupportedEncodingException {
        final String encodedUrl = "http%3A%2F%2Fwww.nueda.com";
        final String decodedUrl = CommonUtil.decodeUrl(encodedUrl);
        assertEquals("http://www.nueda.com",decodedUrl);
    }


}