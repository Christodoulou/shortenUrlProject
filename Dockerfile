FROM openjdk:11
ADD target/shortenurl-0.0.1-SNAPSHOT.jar shortenurl.jar
EXPOSE 8090
ENTRYPOINT ["java","-jar","shortenurl.jar"]