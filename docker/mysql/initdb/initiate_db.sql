CREATE DATABASE IF NOT EXISTS `nueda_shortenurl_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
CREATE USER IF NOT EXISTS 'nueda_shortenurl_usr'@'%' IDENTIFIED BY 'nueda102030';
GRANT ALL PRIVILEGES ON `nueda_shortenurl_db`.* TO 'nueda_shortenurl_usr'@'%';
FLUSH PRIVILEGES;
